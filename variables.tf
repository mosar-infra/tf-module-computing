# computing/variables.tf

variable "public_key_path" {}
variable "ami_name_string" {}
variable "ami_owners" {}
variable "key_name" {}
variable "nodes" {}
variable "lb_target_groups" {}
variable "lb_ec2_tg_attachments" {}
variable "private_subnet_cidr_map" {}
