# computing/main.tf

data "aws_ami" "mosar_server_ami" {
  owners      = var.ami_owners
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_name_string]
  }
}

resource "random_id" "server_id" {
  for_each    = var.nodes
  byte_length = 4
  keepers = {
    key_name = var.key_name
    ami_id   = data.aws_ami.mosar_server_ami.id
  }
}

resource "aws_key_pair" "ssh_key" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

resource "aws_instance" "node" {
  for_each                    = var.nodes
  ami                         = random_id.server_id[each.key].keepers.ami_id
  instance_type               = each.value.instance_type
  key_name                    = random_id.server_id[each.key].keepers.key_name
  subnet_id                   = each.value.subnet_ids[0]
  private_ip                  = each.value.private_ip
  vpc_security_group_ids      = each.value.vpc_security_group_ids
  associate_public_ip_address = each.value.associate_public_ip_address
  iam_instance_profile        = each.value.iam_instance_profile
  root_block_device {
    volume_size = each.value.root_block_device_vol_size
  }

  tags = {
    Name = "${each.key}${random_id.server_id[each.key].dec}"
  }
  user_data = templatefile(each.value.user_data_file_path, each.value.script_vars)
}

resource "aws_lb_target_group_attachment" "node_tg_attachment" {
  for_each         = var.lb_ec2_tg_attachments
  target_group_arn = var.lb_target_groups[each.key].arn
  target_id        = aws_instance.node[each.key].id
  port             = var.lb_target_groups[each.key].port
}

